This module makes it possible to use Firefox with the "Command Line Print"
extension (or some random other tool) to create a PDF of a page.  Given that you
have customized your CSS for printed media, the generated PDFs will look the
same as when you select File -> Print in Firefox 3.6.


MODULE INSTALLATION
————————————————————————————————————————————————————————————————————————————————
 o  Enable the module in your favorite way:

      $ drush -y en ffpdf

 o  Configure it in admin/settings/ffpdf
 o  Grant access to users allowed to generate PDFs in admin/user/permissions
 o  Configure each content type to setup a link in the 'links' area of nodes.
 o  Configure the block with PDF download link in admin/build/block
 o  All drupal paths have a PDF version by attaching the GET parameter ffpdf=1

      http://example.com/your/drupal/path?ffpdf=1


SETTING UP FIREFOX SERVER-SIDE
————————————————————————————————————————————————————————————————————————————————

The following tools are required:

 o  Firefox    — 3.6 or anything else supported by cmdlnprint
 o  cmdlnprint — http://sites.google.com/site/torisugari/commandlineprint2
 o  xvfb       — Create a dummy DISPLAY for firefox
 o  sudo       — You cannot run firefox as the www user

Create a local user for the firefox profile, say 'firefox-print'

  # adduser firefox-print

Install the cmdlnprint extension server-side

 o  First you have to install the extension on your workstation, system wide:
      - Copy the .xpi to your /path/to/firefox/extensions
      - Start firefox (as root?) and confirm the installation
    This will create a new extension path id. Maybe this?

        /path/to/firefox/extensions/{243cc1ee-c821-4544-aebb-54c44125b9aa}

 o  copy the installed extension directory to your webserver:

        $ scp -r /path/to/firefox/extensions/{243cc1ee-c821-4544-aebb-54c44125b9aa} webserver:/usr/local/lib/mozilla/firefox-3.6/extensions

Setup a sudo handler for running firefox as a different user

 o  Copy ffpdf.sudoers to, say, /etc/sudoers.d/ffpdf, and modify it to your
    environment.  Assert /etc/sudoers have an #include directive for
    /etc/sudoers.d

All in all, with sudo, xvfb and firefox, the command for creating a pdf using
cmdlnprint as the user 'www-data' would be:

    sudo -u firefox-print /usr/bin/xvfb-run /usr/local/lib/firefox/firefox-3.6/firefox -print !url -printmode pdf -printfile !dest

Make sure, when configuring this module, that you make the pdf destination
directory world writeable, so the 'firefox-print' user can write to it.


LIMITATIONS
————————————————————————————————————————————————————————————————————————————————

Given the nature of Firefox, only one generation can run simultaneous.  That's a
huge disadvantage on large sites.  Two simultaneous PDF generations will cause
one of the generations to fail.  Therefore, use this tool only on small sites or
limit its access to just a few users.

The URL Firefox sees server side is different from the URL the user has client
side.  So the referenced URL in the printed right side header (default firefox
setting) will be wrong.  That's why the firefox settings in prefs.js don't have
the URL in neither.


TODO
————————————————————————————————————————————————————————————————————————————————

Make this compatible with the print module.
