
;(function($) {

Drupal.behaviors.page2PdfUi = {
  attach: function(context) {
    $('fieldset#edit-page2pdf', context).drupalSetSummary(function(context) {
      return $('[name="page2pdf_link"]:checked', context).val() == 1 ? Drupal.t("Provide a link") : Drupal.t("Do not provide a link");
    });
  }
};

})(jQuery);
