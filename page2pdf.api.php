<?php

/**
 * @file
 * Third party API for Page to PDF.
 */

/**
 * Perform modifications to suggested filename of this page prior to download.
 */
function hook_page2pdf_filename_alter(&$prompted_filename) {
  $prompted_filename = strtoupper($prompted_filename);
}
