<?php

/**
 * @file
 * Drush utilities for Page to PDF.
 */

/**
 * Implements hook_drush_command().
 */
function page2pdf_drush_command() {
  $items = array();

  $items['page2pdf-create'] = array(
    'description' => "Generate a pdf of given (drupal) path.",
    'arguments' => array(
      'path' => "Drupal path. Aliases are also accepted. Path should *not* begin with '/'",
    ),
    'options' => array(
      'dest' => array(
        'description' => "Destination as stream wrapper uri",
        'value' => 'required',
        'example-value' => 'private://page2pdf',
      ),
      'replace' => array(
        'description' => "Replace existing file",
      ),
    ),
    'aliases' => array('p2pc'),
    'drupal dependencies' => array('page2pdf'),
  );
  return $items;
}

/**
 * Create a PDF of given drupal path.
 */
function drush_page2pdf_create($path) {
  $dest = drush_get_option('dest');
  $dest = $dest ?: 'public://';
  $replace = drush_get_option('replace') ? FILE_EXISTS_REPLACE : FILE_EXISTS_RENAME;
  $pdf = page2pdf_path_to_pdf($path, $dest, $replace);
  if ($pdf) {
    drush_print(t("PDF of page @path saved in @dest", array('@path' => $path, '@dest' => $pdf->uri)));
  }
  else {
    watchdog('page2pdf', "Failed to generate PDF of page !path", array('!path' => $path), WATCHDOG_WARNING);
  }
}

/**
 * Implements hook_drush_cache_clear().
 */
function page2pdf_drush_cache_clear(&$types) {
  $types['page2pdf'] = 'page2pdf_cache_clear';
}
