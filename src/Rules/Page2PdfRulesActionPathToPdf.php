<?php

/**
 * @file
 * Rule action to generate PDF from Drupal internal path.
 */

/**
 * Implements RulesActionHandlerInterface.
 */
class Page2PdfRulesActionPathToPdf extends RulesActionHandlerBase implements RulesActionHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'page2pdf_rules_path_to_pdf',
      'description' => t("Action to generate drupal internal path to PDF."),
      'label' => t("Path to PDF"),
      'group' => t('Page to PDF'),
      'parameter' => array(
        'path' => array(
          'type' => 'text',
          'label' => t("Path"),
          'description' => t("Drupal internal path to item that should be generated as PDF. <strong>Note!</strong> Make sure this rule isn't triggered by the path entered here as this will cause an endless recursion."),
        ),
        'destination' => array(
          'type' => 'text',
          'label' => t("Destination"),
          'description' => t("Destination file or directory relative to 'files' folder or as a valid file stream wrapper."),
        ),
      ),
      'provides' => array(
        'pdf' => array(
          'type' => 'file',
          'label' => t("Generated PDF"),
          'wrapped' => TRUE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($path, $destination) {
    $destination = file_uri_scheme($destination) ? file_stream_wrapper_uri_normalize($destination) : file_build_uri($destination);
    $pdf = page2pdf_path_to_pdf($path, $destination);
    return array('pdf' => $pdf);
  }

}
